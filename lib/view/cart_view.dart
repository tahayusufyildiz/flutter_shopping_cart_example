import 'package:flutter/material.dart';
import 'package:flutter_cart/flutter_cart.dart';

class CartView extends StatelessWidget {
  final FlutterCart? cardList;
  CartView({
    Key? key,
    this.cardList,
  }) : super(key: key);

  var cart = FlutterCart();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Text(cardList!.cartItem[0].productName.toString()),
    ));
  }
}
