import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_cart/flutter_cart.dart';
import 'package:flutter_shopping_cart_example/model/product_model.dart';
import 'package:flutter_shopping_cart_example/view/cart_view.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var cart = FlutterCart();

  Future<List<ProductsModel>> getProducts() async {
    final url = Uri.parse("https://fakestoreapi.com/products");
    final response = await http.get(url);
    var responseData = json.decode(response.body);

    List<ProductsModel> products = [];
    for (var singleproduct in responseData) {
      ProductsModel product = ProductsModel(
        id: singleproduct["id"],
        title: singleproduct["title"],
        description: singleproduct["description"],
        image: singleproduct["image"],
      );
      products.add(product);
    }
    return products.toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: IconButton(
              onPressed: () {
                Get.to(CartView());
              },
              icon: Icon(
                Icons.shopping_cart,
                size: 40,
              ),
            ),
          ),
        ],
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: FutureBuilder(
          future: getProducts(),
          builder: (context, snapshot) {
            if (snapshot.data == null) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return ListView.builder(
                itemCount: snapshot.data?.length,
                itemBuilder: (context, index) => Padding(
                  padding: EdgeInsets.all(12),
                  child: Column(
                    children: [
                      Container(
                          height: 200,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Colors.blue.shade200,
                            borderRadius: BorderRadius.circular(12),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Container(
                                      width: 100,
                                      height: 100,
                                      child: Image.network(snapshot
                                          .data![index].image
                                          .toString()),
                                    ),
                                    VerticalDivider(
                                      thickness: 1,
                                      color: Colors.orange,
                                    ),
                                    Container(
                                      width: 100,
                                      height: 100,
                                      child: SingleChildScrollView(
                                        child: Column(
                                          children: [
                                            Text(
                                              snapshot.data![index].title
                                                  .toString(),
                                              style: TextStyle(
                                                  color: Colors.blue,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                ElevatedButton(
                                  onPressed: () {
                                    cart.addToCart(
                                        productId: snapshot.data![index].id,
                                        unitPrice: 10,
                                        productName:
                                            snapshot.data![index].title);
                                  },
                                  child: Text("Add a cart"),
                                )
                              ],
                            ),
                          )),
                    ],
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
